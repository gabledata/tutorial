# Tutorial: Getting Started with Gable Platform

Welcome to the tutorial for Gable! In this tutorial, you will:

- Set up a private tutorial repository with Gable and Gitlab jobs
- Validate and publish a new data contract
- Attempt to make a breaking change to a data asset under contract

This tutorial utilizes Gable's Gitlab hidden jobs to validate & publish data contracts, register data assets, and detect contract violations all using the Gable platform. If you would like to run a similar tutorial using just the CLI, you can check out the [CLI tutorial](./docs/cli.md).

## Step 1: Set up a forked tutorial repository

The tutorial repository is a small, self contained sample repository that lets you run through the end-to-end process of publishing a data contract and seeing that contract be enforced with Gable's platform. In this tutorial, you'll create a private copy of the repository, configure your Gable credentials, and run the CI/CD workflows in Gitlab pipelines. If you're using the credentials from your sandbox environment, you can even run through this tutorial by making a private copy of this repository.

### Create a private copy of the tutorial repository

1. Navigate to the [the Gitlab Projects page](https://gitlab.com/dashboard/projects) and click on the "New Project" button in the top-right, then choose "Import project"
2. Choose the "Repository by URL" option
3. Fill in https://gitlab.com/gabledata/tutorial.git for the "Git repository URL", and select "Mirror repository". For the "Project URL", you can select your personal account, or your company's group.

![Import Project](./static/gable_gitlab_import_project.png)

### Get Your API Key

In order to connect the Github Actions to Gable, you need:

- The API endpoint associated with your organization
- An API key that corresponds to the endpoint

You can find your API endpoint and key by navigating to the `Settings -> API Keys` page of Gable. Under API Keys you can click `View` to reveal your API key.

![Gable API Keys](./static/gable_settings_api_keys_page_example.png)

### Setting up GABLE_API_KEY and GABLE_API_ENDPOINT variables

Next, you will need to create the `GABLE_API_KEY` and `GABLE_API_ENDPOINT` project variables in your repository's CI/CD settings to configure Github Actions to talk to Gable.

Follow the Gitlab instructions for [Defining a CI/CD variable in your project](https://docs.gitlab.com/ee/ci/variables/#for-a-project) to create the `GABLE_API_KEY`and `GABLE_API_ENDPOINT` repository variables.

![CICD Variables](./static/gable_gitlab_cicd_variables.png)

### Create MR Comment Token

The MR comment token allows Gable's CI/CD checks to comment on MRs that will cause contract violations.

Follow the Gitlab instructions for [Creating a Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token). 

Name the token `GABLE_MR_COMMENT_TOKEN`, select "Reporter" as the role, and select the `api` scope. You can optionally clear the expiration date.

![Gable MR Comment Token](./static/gable_gitlab_mr_comment_token.png)

Copy the value of the token after you create it, and save it somewhere secure. Finally, go add an additional project variable similar to the variables added in the `Setting up GABLE_API_KEY and GABLE_API_ENDPOINT variables` section above.

### Trigger Data Asset Registration

For the tutorial, there are multiple types of data assets including event schemas, database tables, and tables created from a PySpark job. In order to kick off the first workflow to register the data assets, you'll need to push an empty commit to the `main` branch of the repository. Run:

```bash
git commit --allow-empty -m "Run main pipeline"
git push
```

The pipeline will start after you push the commit, and once it completes, you should be able to see the new data assets in the Gable UI.

Congratulations! You've set up your tutorial repository and are ready to try out Gable's platform!

## Step 2: Creating Your First Data Contract

The tutorial repository includes many different data assets for a generic e-commerce company. It includes database tables, event schemas, and a PySpark job. For this tutorial, we are going to create a contract on the `order_details` database table.

### Create a new branch to add the contract

You are going to create a new data contract in the tutorial repository. Just like code changes, rather than creating the contract in the `main` branch of the repository, it's best practice to create a new branch, and open a Pull Request for the changes. The PR allows others to comment on your changes, and also allows the Gable Gitlab jobs to validate the contract is syntactically correct before pushing changes.

This tutorial will walk through creating the branch using the git command line. In the tutorial repository on your local machine:

1. **Open a Terminal Window**: Navigate to the directory where your cloned tutorial repository is located
2. **Checkout a New Branch**: Use the following command to create and switch to a new branch called `first_contract`:

   ```bash
   git checkout -b first_contract
   ```

Great! Now you can start writing the contract!

### Write the data contract

You are going to create a data contract for the `order_details` table of the database, which represents the individual items included in a customer order. Writing a data contract involves creating a YAML file that declares the schema and semantics of the data following the data contract specification.

Contracts are associated with a specific data asset. When you first triggered a pipeline run, it created several data assets for database tables and event schemas. You can navigate to the `Data Assets` page to view a list of your organization's assets.

![Gable Data Assets](./static/gable_data_asset_list.png)

You can click on the data asset where you can view its details including the ID Gable uses for the asset. You can click on the clipboard next to the ID to copy it. For this tutorial, we are using the `tutorial.public.order_details` data asset.

![Gable Data Asset Details](./static/gable_data_asset_detail.png)

In the `contracts` directory of your local repository, create a file called `order_details.yaml`. **NOTE: The CLI can upload both `.yaml` and `.yml` files but the tutorial is set up to only upload `.yaml` files. Please ensure that the file extension is `.yaml` for the purposes of this tutorial**.

Copy and paste the following into the contents of `order_details.yaml`:

```yaml
id: 7b7eabe6-a52a-4e19-8bf3-9015eadbbed0
dataAssetResourceName: postgres://prod.store.com:5432:tutorial.public.order_details
spec-version: 0.1.0
name: OrderDetails
namespace: Tutorial
doc: Details of items for an order. Each row represents the different items that were part of an order including their quantity and price.
owner: chadgable@gable.ai
schema:
  - name: order_id
    doc: The identifier of the order the item is associated with
    type: int32
  - name: product_id
    doc: The identifier of the product that was ordered
    type: int32
  - name: quantity
    doc: The quantity of the product in the order
    type: int32
  - name: total_price
    doc: The total price of the item in the order
    type: decimal256
  - name: vendor_id
    doc: The identifier of the vendor that the product was ordered from
    type: int32
```

This contract contains information on what data the contract applies to, who owns the contract, as well as the minimum expected schema for the data from the `order_details` table.

Remark: You can generate a Data Contract template for a Data Asset using the Gable UI.

![Genarate Contract from Data Asset](./static/gable_data_asset_generate_contract.png)

### Push Your Changes to Gitlab

Now that you have created the contract, it is time to commit the change to the repository. To stage and commit your changes, run:

```bash
git add .
git commit -m "Add order details data contract"
git push origin first_contract
```

### Validate the data contract

Before merging your changes back to the `main` branch, it is a good idea to create a Merge Request. Creating the Pull Request will serve two purposes:

- Allow others to review your newly-created data contract
- Allow the Gitlab job to validate the data contract is syntactically correct

Once your MR is open, Gable's contract validation CI/CD check runs automatically to check for syntax errors in your data contracts. You can open the MR pipeline to see the status, if there were any errors in your contract they would appear here.

![Validate Data Contracts Check Results](./static/validate_contracts_check_results.png)

### Publish the Data Contract

Merge the MR you opened. This will kick off the Gitlab job which publishes the contract.

Once the Gitlab job on the `main` branch has been run and the data contract has been published, navigate to the Gable UI and you should see your new data contract!

## Step 3: Preventing Breaking Data Changes

**NOTE**: This section relies on the `order_details` data contract created in the previous step. Please complete that step if you have not already.

Now that there is a data contract in place for the `order_details` data, every time a change affecting the underlying `order_details` table, Gable checks to ensure that changes do not violate the contract. Let's try making a breaking change to the `order_details` table.

### Create a new branch

First off, pull changes down from the main branch to ensure that your repository is up-to-date. Create a new branch called `breaking_data_change`:

```bash
git checkout main
git pull
git checkout -b breaking_data_change
```

### Make a Breaking Change

We are going to add a new migration file for our database to attempt to break the contract.

Create a new file called `7a341d712aa5_order_detail_changes.py` in the `./db_migrations/versions/` [folder](./db_migrations/versions/). Paste the following contents into the new `7a341d712aa5_order_detail_changes.py` file:

```python
"""order_detail changes

Revision ID: 7a341d712aa5
Revises: 78da3a78bfd6
Create Date: 2023-09-29 13:22:35.204135

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '7a341d712aa5'
down_revision: Union[str, None] = '78da3a78bfd6'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # Make the vendor optional
    op.alter_column(
        'order_details',  
        'vendor_id',      
        nullable=True,    
    )

    # Convert existing values to integers (for example, if representing cents)
    op.execute('UPDATE order_details SET unit_price = ROUND(unit_price * 100)')
    op.execute('UPDATE order_details SET total_price = ROUND(total_price * 100)') 
    
    # Alter the column type
    op.alter_column('order_details', 'unit_price', type_=sa.Integer)
    op.alter_column('order_details', 'total_price', type_=sa.Integer)


def downgrade() -> None:
    op.alter_column(
        'order_details',
        'vendor_id',
        nullable=False,
    )

    # Alter the column type back to Numeric
    op.alter_column('order_details', 'unit_price', type_=sa.Numeric(10, 2))
    op.alter_column('order_details', 'total_price', type_=sa.Numeric(10, 2))

    # Convert integer values back to their decimal representations
    op.execute('UPDATE order_details SET unit_price = total_price / 100.0')
    op.execute('UPDATE order_details SET total_price = total_price / 100.0')
```

This migration file does a couple of things. First, it converts the `unit_price` and `total_price` columns to an integer field and makes the `vendor_id` optional.

Both the `total_price` and `vendor_id` columns are protected in the contract. Changing the type of `total_price` and making `vendor_id` represent breaking changes.

Once the change has been saved, commit the change and push it to Gitlab:

```bash
git add .
git commit -m "Update order details table"
git push origin breaking_data_change
```

### Open a Merge Request

Open a Merge Request for the proposed breaking change

### View Warning Message

The Gitlab job will validate the changes against existing data contracts. It will detect the change to the `order_details` table and post a message in the MR that this change breaks the existing data contract.

![Contract Violation PR Comment](./static/pr_comment_contract_violation.png)

## Further Reading

Congratulations on creating your first data contract and validating data asset changes! Be sure to check out more of Gable's documentation for more information on our platform!
